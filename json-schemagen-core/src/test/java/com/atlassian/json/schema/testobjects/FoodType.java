package com.atlassian.json.schema.testobjects;

public enum FoodType
{
    MEAT, VEGETABLE
}
