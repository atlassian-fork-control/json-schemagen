package com.atlassian.json.schema.model;

import java.util.Map;
import java.util.Set;

public class AbstractSchema implements JsonSchema
{
    private String id;
    private String $ref;
    private String type;
    private String title;
    private String description;
    private String fieldTitle;
    private String fieldDescription;
    private Set<JsonSchema> allOf;
    private Set<JsonSchema> anyOf;
    private Set<JsonSchema> oneOf;
    private JsonSchema not;
    private Map<String,ObjectSchema> definitions;
    private String defaultValue;
    
    @Override
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Override
    public String getRef()
    {
        return $ref;
    }

    public void setRef(String $ref)
    {
        this.$ref = $ref;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String getFieldTitle()
    {
        return fieldTitle;
    }

    @Override
    public String getFieldDescription()
    {
        return fieldDescription;
    }

    @Override
    public void setFieldTitle(String title)
    {
        this.fieldTitle = title;
    }

    @Override
    public void setFieldDescription(String description)
    {
        this.fieldDescription = description;
    }

    @Override
    public Set<JsonSchema> getAllOf()
    {
        return allOf;
    }

    public void setAllOf(Set<JsonSchema> allOf)
    {
        this.allOf = allOf;
    }

    @Override
    public Set<JsonSchema> getAnyOf()
    {
        return anyOf;
    }

    public void setAnyOf(Set<JsonSchema> anyOf)
    {
        this.anyOf = anyOf;
    }

    @Override
    public Set<JsonSchema> getOneOf()
    {
        return oneOf;
    }

    public void setOneOf(Set<JsonSchema> oneOf)
    {
        this.oneOf = oneOf;
    }

    @Override
    public JsonSchema getNot()
    {
        return not;
    }

    public void setNot(JsonSchema not)
    {
        this.not = not;
    }

    @Override
    public Map<String,ObjectSchema> getDefinitions()
    {
        return definitions;
    }

    public void setDefinitions(Map<String,ObjectSchema> definitions)
    {
        this.definitions = definitions;
    }

    @Override
    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }
}
