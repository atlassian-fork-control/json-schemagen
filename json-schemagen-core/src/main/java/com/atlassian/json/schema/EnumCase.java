package com.atlassian.json.schema;

public enum EnumCase
{
    UPPER,LOWER,INSENSITIVE
}
