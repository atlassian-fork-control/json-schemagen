package com.atlassian.json.schema.model;

public class BasicSchema extends AbstractSchema
{
    private String $schema;

    public String getDollarSchema()
    {
        return $schema;
    }

    public void addDollarSchema(String schemaUri)
    {
        this.$schema = schemaUri;
    }
}
