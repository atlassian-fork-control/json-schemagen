package com.atlassian.json.schema;

public class DollarSchema
{
    public static final String CURRENT = "http://json-schema.org/schema#";
    public static final String CURRENT_HYPER = "http://json-schema.org/hyper-schema#";
    public static final String DRAFTV4 = "http://json-schema.org/draft-04/schema#";
    public static final String DRAFTV4_HYPER = "http://json-schema.org/draft-04/hyper-schema#";
    public static final String DRAFTV3 = "http://json-schema.org/draft-03/schema#";
    public static final String DRAFTV3_HYPER = "http://json-schema.org/draft-03/hyper-schema#";
}
