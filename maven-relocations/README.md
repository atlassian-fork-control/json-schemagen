These are Maven relocation POM files as per http://maven.apache.org/guides/mini/guide-relocation.html
They are useful for users who would update their dependency on json-schemagen without knowing there is
a new version, or when they are using LATEST. The new groupID is `io.atlassian.json-schemagen` - while
the old groupId was `external.atlassian.json`
