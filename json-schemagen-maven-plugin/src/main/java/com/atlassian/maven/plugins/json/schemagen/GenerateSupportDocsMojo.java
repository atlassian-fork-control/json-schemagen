package com.atlassian.maven.plugins.json.schemagen;

import com.atlassian.json.schema.doclet.JsonSchemaDoclet;
import com.atlassian.json.schema.scanner.InterfaceImplementationScanner;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;

@Mojo(name = "generate-support-docs", requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateSupportDocsMojo extends AbstractSchemaGenMojo
{

    @Component
    private BuildPluginManager buildPluginManager;

    @Component
    private MavenSession session;

    @Parameter
    private String basePackage = "";

    @Parameter(property = "sourcepath", defaultValue = "${project.basedir}/src/main/java")
    private String sourcepath = "";

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        runJavadoc();
        runInterfaceScanner();
    }

    private void runInterfaceScanner() throws MojoExecutionException
    {
        InterfaceImplementationScanner scanner = new InterfaceImplementationScanner();
        try
        {
            scanner.scan(basePackage, getProjectClassLoader(), new File(getDefaultInterfacesFile()));
        }
        catch (Exception e)
        {
            throw new MojoExecutionException("Error running interface scanner", e);
        }
    }

    private void runJavadoc() throws MojoExecutionException
    {
        String resourcedocPath = getDefaultDocsFile();

        String packagePath = "**" + File.separator + basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator)) + File.separator + "**" + File.separator + "*.java";

        MojoExecutor.Element jflags = null;

        if(debug)
        {
            jflags = element(name("additionalJOptions"),
                        element(name("additionalJOption"),"-J-Xdebug")
                        ,element(name("additionalJOption"),"-J-Xnoagent")
                        ,element(name("additionalJOption"),"-J-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005")
                    );
        }

        final Xpp3Dom configuration = configurationWithoutNullElements(
                element(name("maxmemory"), "1024m"),
                element(name("doclet"), JsonSchemaDoclet.class.getName()),
                // doclet is in schemagen-scanner
                element(name("docletPath"), getPluginAndProjectClasspath()),
                element(name("additionalparam"), "-output \"" + resourcedocPath + "\""),
                element(name("sourceFileIncludes"), element(name("sourceFileInclude"), packagePath)),
                // We already have project dependencies in docletPath, shouldn't need more here
                // element(name("additionalDependencies"), createAdditionalDependenciesElements()),
                element(name("sourcepath"), sourcepath),
                element(name("quiet"), String.valueOf(!debug)),
                element(name("show"), "private"),
                element(name("useStandardDocletOptions"), "false"),
                jflags
        );
        getLog().debug("Invoking maven-javadoc-plugin with " + configuration);
        executeMojo(
                plugin(
                        groupId("org.apache.maven.plugins"),
                        artifactId("maven-javadoc-plugin"),
                        version("2.9.1")
                ),
                goal("javadoc"),
                configuration,
                executionEnvironment()
        );
    }

    /**
     * Returns a list of Mojo executor <pre>additionalDependency</pre> elements for the project from which the schema
     * generator was invoked.
     *
     * NOTE: system scope dependencies are skipped since <pre>maven-javadoc-plugin</pre> does not accept them.
     *
     * @return a list of additional dependencies
     * @throws MojoExecutionException if an error occurs in dependency retrieval
     */
    private MojoExecutor.Element[] createAdditionalDependenciesElements() throws MojoExecutionException
    {
        return project.getDependencyArtifacts().stream()
                .filter(artifact -> !Artifact.SCOPE_SYSTEM.equals(artifact.getScope()))
                .map(artifact -> element(name("additionalDependency"),
                        element(name("groupId"), artifact.getGroupId()),
                        element(name("artifactId"), artifact.getArtifactId()),
                        element(name("version"), artifact.getVersion()),
                        element(name("classifier"), artifact.getClassifier()),
                        element(name("type"), artifact.getType())))
                .toArray(Element[]::new);
    }

    protected MojoExecutor.ExecutionEnvironment executionEnvironment()
    {
        return MojoExecutor.executionEnvironment(project, session, buildPluginManager);
    }

    protected static Xpp3Dom configurationWithoutNullElements(MojoExecutor.Element... elements)
    {
        List<Element> nonNullElements = new ArrayList<Element>();
        for (MojoExecutor.Element e : elements)
        {
            if (e != null)
            {
                nonNullElements.add(e);
            }
        }

        return MojoExecutor.configuration(nonNullElements.toArray(new MojoExecutor.Element[nonNullElements.size()]));
    }

}
