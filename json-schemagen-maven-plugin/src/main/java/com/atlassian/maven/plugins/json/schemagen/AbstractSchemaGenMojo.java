package com.atlassian.maven.plugins.json.schemagen;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractSchemaGenMojo extends AbstractMojo
{

    @Component
    protected MavenProject project;

    @Parameter(defaultValue = "false")
    protected Boolean debug = false;

    @Parameter( readonly = true, defaultValue = "${plugin.version}" )
    private String pluginVersion;

    @Parameter( readonly = true, defaultValue = "${plugin.groupId}" )
    private String pluginGroupId;

    @Parameter( readonly = true, defaultValue = "${plugin.artifacts}" )
    private List<Artifact> pluginDependencies;

    protected String getDefaultDocsFile()
    {
        StringBuilder outBuilder = new StringBuilder(project.getBuild().getDirectory());
        outBuilder.append(File.separator).append("jsonSchemaDocs.json");

        return outBuilder.toString();
    }

    protected String getDefaultInterfacesFile()
    {
        StringBuilder outBuilder = new StringBuilder(project.getBuild().getDirectory());
        outBuilder.append(File.separator).append("jsonSchemaInterfaces.json");

        return outBuilder.toString();
    }

    protected Stream<File> getProjectClasspath()
    {
        return Stream.concat(
                Stream.of(new File(project.getBuild().getOutputDirectory())),
                project.getArtifacts().stream()
                        .filter(this::scope)
                        .map(Artifact::getFile));
    }

    protected URL[] getProjectClasspathURLs()
    {
        return getProjectClasspath()
                .map(this::toURL)
        .toArray(URL[]::new);

    }

    protected ClassLoader getProjectClassLoader()
    {
        return new URLClassLoader(getProjectClasspathURLs(), getClass().getClassLoader());
    }

    protected String getPluginAndProjectClasspath()
    {
        return Stream.concat(
                getProjectClasspath().map(File::getPath),
                pluginDependencies.stream()
                        .filter(this::scope)
                        .map(artifact -> artifact.getFile().getPath()))
                .collect(Collectors.joining(File.pathSeparator));
    }

    private boolean scope(Artifact a) {
        return Artifact.SCOPE_COMPILE.equals(a.getScope()) || Artifact.SCOPE_PROVIDED.equals(a.getScope()) || Artifact.SCOPE_RUNTIME.equals(a.getScope());
    }

    private URL toURL(File f) {
        try {
            return f.toURI().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

}
