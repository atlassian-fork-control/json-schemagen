# New groupID !

Please note that with version 1.2.0, the Maven `groupId` for this project has changed from `external.atlassian.json`
to `io.atlassian.json-schemagen`. We tried to get Maven relocations to work, with little success: there seems to be a
bug when using that with plugins. We might publish the relocations anyway, but no promises at this stage. Just change
to the new groupID!

# JSON SchemaGen

JSON SchemaGen is a library that allows generating a [JSON-Schema](http://json-schema.org/) by annotating the same Java
classes used for your JSON representations.

Essentially, the same classes you use to consume JSON can also be used to generate a JSON-Schema.

## Usage
With Maven, add the plugin and configure it, e.g:
````
            <plugin>
                <groupId>io.atlassian.json-schemagen</groupId>
                <artifactId>json-schemagen-maven-plugin</artifactId>
                <version>1.2.0</version>
                <executions>
                    <execution>
                        <id>generate-schema</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>generate-schema</goal>
                        </goals>
                        <configuration>
                            <!-- Optional: <generatorProvider>your.impl.of.com.atlassian.json.schema.JsonSchemaGeneratorProvider</generatorProvider> -->
                            <rootClassName>org.foo.Bar</rootClassName>
                            <rawOutput>${project.build.outputDirectory}/schema/my-schema.json</rawOutput>
                            <prettyOutput>${project.build.outputDirectory}/schema/my-schema-pretty.json</prettyOutput>
                            <enumCase>insensitive</enumCase>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
````
